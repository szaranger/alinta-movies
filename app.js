'use strict';

$(function() {
  var API = 'http://alintacodingtest.azurewebsites.net/api/Movies?callback=?';
  var actors =[];

  var payload = [
     {
        "name":"Beverly Hills Cop",
        "roles":[
           {
              "name":"Axel Foley",
              "actor":"Eddie Murphy"
           },
           {
              "name":"Billy Rosewood",
              "actor":"Judge Reinhold"
           },
           {
              "name":"Sgt. Taggart",
              "actor":"John Ashton"
           },
           {
              "name":"Jenny Summers",
              "actor":"Lisa Eilbacher"
           },
           {
              "name":"Mikey Tandino",
              "actor":""
           }
        ]
     },
     {
        "name":"Stand By Me",
        "roles":[
           {
              "name":"Gorgie Lachance",
              "actor":"Wil Wheaton"
           },
           {
              "name":"Chris Chambers",
              "actor":"River Phoenix"
           },
           {
              "name":"Teddy Duchamp",
              "actor":"Corey Feldman"
           },
           {
              "name":"Ace Merrill",
              "actor":"Keifer Sutherland"
           },
           {
              "name":"The Writer",
              "actor":"Richard Dreyfuss"
           }
        ]
     },
     {
        "name":"Star Trek",
        "roles":[
           {
              "name":"Romulan",
              "actor":"Wil Wheaton"
           },
           {
              "name":"Kirk",
              "actor":"Chris Pine"
           },
           {
              "name":"Nero",
              "actor":"Eric Bana"
           },
           {
              "name":"Spock",
              "actor":"Leonard Nimoy"
           },
           {
              "name":"Scotty",
              "actor":"Simon Pegg"
           },
           {
              "name":"Amanda Grayson",
              "actor":"Winona Ryder"
           }
        ]
     },
     {
        "name":"Family Guy",
        "roles":[
           {
              "name":"Meg Griffin",
              "actor":"Mila Kunis"
           },
           {
              "name":"Meg Griffin",
              "actor":"Mila Kunis"
           },
           {
              "name":"Chris Griffin",
              "actor":"Seth Green"
           },
           {
              "name":"Luke Skywalker",
              "actor":"Seth Green"
           },
           {
              "name":"Joe Swanson"
           },
           {
              "name":"Lois Griffin",
              "actor":"Alex Borstein"
           }
        ]
     }
  ];

  payload.forEach(function(movie) {
    movie.roles.forEach((role) => {
      var actor = search(role.actor, actors, 'actor');

      if (actor) {
        var names = actor.names;
        var movies = actor.movies;

        if (names) {
          names ? names.push(role.name) : names = [role.name];
        } else {
          names.push(role.name);
        }

        if (movies) {
          movies ? movies.push(movie.name) : movies = [movie.name];
        } else {
          movies.push(movie.name);
        }
      } else {
        actors.push({ actor: role.actor, names: [role.name], movies: [movie.name] });
      }
    });
  });

  $('.movies').append(createComponent(actors));

  function search(nameKey, arrayToSearch, attr){
    for (var i = 0; i < arrayToSearch.length; i++) {
      if (arrayToSearch[i][attr] === nameKey) {
          return arrayToSearch[i];
      }
    }
  }

  function reorder(a, b) {
    return b - a;
  }

  function createComponent(items) {
    var article = document.createElement('article');

    items.forEach(function(item) {
      var h2 = document.createElement('h2');
      h2.innerText = item.actor;
      article.appendChild(h2);

      var ordered = item.movies.sort(reorder);

      ordered.forEach(function(movie, index) {
        var p = document.createElement('p');
        p.innerText = movie + ' (' + item.names[index] + ')';
        article.appendChild(p);
      });
    });

    return article;
  }
});
